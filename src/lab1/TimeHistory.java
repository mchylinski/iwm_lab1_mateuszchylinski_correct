package lab1;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TimeHistory extends Sequence

{
private double sensitivity;

	public TimeHistory(String device, String description, ZonedDateTime date, double sensitivity) 
	{
		super(device, description, date);
		this.sensitivity = sensitivity;
		
		
	}
	
	public String getNames()
	{
		String exit = "Sensitivity:  "+  String.valueOf(sensitivity) ;
		return exit;
		
	}
	
}

