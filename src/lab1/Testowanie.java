package lab1;

import java.time.ZonedDateTime;

import lab1.Spectrum.scalling;

public class Testowanie {

	public static void main(String[] args) 
	{ 
		Alarm myAlarm = new Alarm("Machine", "DC motor 12V", ZonedDateTime.now(),2, 2.112f, 1);
		TimeHistory newTimeHistory = new TimeHistory("Machine", "DC motor 12V", ZonedDateTime.now(), (double)0.005);
		Spectrum newSpectrum = new Spectrum("Machine", "DC motor 12V", ZonedDateTime.now(), scalling.dec);
		
		String alarmName = myAlarm.getNames();
		String timeHistoryName = newTimeHistory.getNames();
		String scallinName = newSpectrum.getNames();
		
		System.out.println(alarmName);
		System.out.println(timeHistoryName);
		System.out.println(scallinName);
	}

}
