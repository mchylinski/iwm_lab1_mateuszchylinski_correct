package lab1;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Packet implements Serializable
{
	protected String device;
	protected String description;
	protected ZonedDateTime date;	
	
	public Packet (String device, String description, ZonedDateTime date)
	{
		this.device  = device;
		this.description  = description;
		this.date  = date;
	}
	
	String getDevice()
	{
	return device;	
	}
	String getDescription()
	{
	return description;	
	}
	ZonedDateTime getDate()
	{
	return date;	
	}
	
}
